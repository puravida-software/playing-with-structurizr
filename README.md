# playing-with-structurizr

## Structurizr DSL

Structurizr es un proyecto de código abierto destinado a generar diagramas de arquitectura usando C4

Para más info visita la página https://github.com/structurizr/cli/blob/master/docs/getting-started.md#2-create-a-software-architecture-model-with-the-dsl

## Kroki

Kroki es un proyecto de código abierto destinado a generar diagramas a partir de texto con una simple llamada HTTP

Para más info visita la página https://kroki.io/

## Proyecto

Este proyecto une estas dos herramientas para que puedas generar tus diagramas en local sin tener que instalar ninguna herramienta 
extra salvo docker y docker-compose

(Además de generar tus diagramas de arquitectura en tu equipo local puedes adaptarlo fácilmente a tu sistema de despliegue contínuo tipo 
Jenkins, Github Actions o Gitlab Pipeline)

## Build

Una vez tengas instalado de docker y docker-compose en tu sistema ejecuta desde el directorio principal del proycto

`make run`

esta orden levantará el container `Kroki` , ejecutará el parseador de Structurizr a PlantUML y un script los enviará a Kroki para convertirlos en PNG

## Ejemplo

Estos son algunos ejemplos generados

![Overview](images/Overview.png)

![API](images/API.png)

![Transactions](images/Transactions.png)

## DSL

El DSL se encuentra en el directorio `src` y en él se han definido los componentes de la arquitectura.

Por ejemplo:

```
 ...
    enterprise "My Company"{
        supportStaff = person "Customer Service Staff" "Customer service staff." "Staff"
        backoffice = person "Back Office Staff" "Administration and support staff within the bank."

        bff = softwaresystem "BFF" "Backend For Front"

        security = softwaresystem "Security" "Login&Roles"

        api = softwaresystem "API" "API Layer"{
            core = container "API"
            database = container "Database" "Stores users" 
        }
...
    } 
...
```

## Generar imágenes

Una vez exportado el DSL a ficheros PlantUML ejecutamos un groovy script que envía cada uno de ellos al container Kroki 
mediante un http POST y vuelca la imagen generada por este en el directorio images

## docker-compose

El fichero docker-compose unifica todas las partes

```
version: "3"
services:
  clean:
    image: busybox:latest
    volumes:
      - .:/root/data
    command: /bin/rm -rf /root/data/images /root/data/puml
  kroki:
    image: yuzutech/kroki        
  export:
    image: ghcr.io/aidmax/structurizr-cli-docker:latest
    volumes:
      - .:/root/data
    command: export -workspace /root/data/src/my_company.dsl -o /root/data/puml  -format plantuml
  generate:
    image: groovy:3.0.6-jre11
    depends_on: 
      - export
      - kroki
    volumes:
      - .:/home/groovy
    command: groovy generate.groovy
```

