workspace "MyCompany" "MyCompany overview" {

    model {
        user = person "User" "A customer."

        enterprise "My Company"{
            supportStaff = person "Customer Service Staff" "Customer service staff." "Staff"

            bff = softwaresystem "BFF" "Backend For Front"

            security = softwaresystem "Security" "Login&Roles"

            api = softwaresystem "API" "API Layer"{
                core = container "API"
                database = container "Database" "Stores users" 
            }

            customerArea = softwaresystem "Customer Area" "All related with customers"{
                customer = container "CustomerService" 
                user_stats = container "UserStats" 
            }

            !include transactions.dsl
        } 

        user -> bff "Uses"
        bff -> security "Auth JWT"
        bff -> core "Business Logic"
        bff -> customerArea "/customer-service"
        core -> transactions "/transactions"

        customer -> user_stats "Notify"
        db -> pg "Order Payment"

        supportStaff -> customerArea "Web"
    }

    views {
        systemContext bff "Overview" {
            include *
            include transactions
            include supportStaff
            autoLayout
        }

        container api "API"{
            include *
            autoLayout
        }

        container transactions "Transactions"{
            include api
            include *
            autoLayout
        }

        container customerArea "CustomerService"{
            include *
            autoLayout
        }
        
        styles {
            element "Person" {
                background #08427b
                color #ffffff
                fontSize 22
                shape Person
            }            
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Existing System" {
                background #999999
                color #ffffff
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "Database" {
                shape Cylinder
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
        }
    }
    
}