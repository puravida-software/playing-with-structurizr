transactions = softwaresystem "Transactions" "All related with money"{
    auth = container "AuthService"
    pg = container "Payments"
    db = container "Debits"
    cards = container "Transactions"
}