up:
	docker-compose up -d

clean:
	docker-compose run clean
	
export:
	docker-compose run export

generate:
	docker-compose run generate

run: up clean export generate	