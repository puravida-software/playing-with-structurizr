@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')

import static groovyx.net.http.HttpBuilder.configure
import static groovyx.net.http.optional.Download.*
import groovy.io.FileType

http = configure {
    request.uri = 'http://kroki:8000'
    request.contentType = 'text/plain'
    request.accept = 'image/png'
    request.headers['User-Agent']='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'
}

root = new File('images')
root.mkdirs()

new File('puml').eachFileMatch(FileType.ANY, ~/.*\.puml/) { f->
    name = f.name.split('\\.')[0..-2].join('')
    name -= 'structurizr-'
    extension = "png"    
    fImage = new File(root, "${name}.${extension}")      
    http.post{
        request.uri = "/plantuml"
        request.body = f.text
        toFile(delegate, fImage)
   }
}